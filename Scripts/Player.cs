using Godot;
using System;

public class Player : KinematicBody2D
{
    private Vector2 movement = Vector2.Zero;
	private float move_speed = 400f;
	private float gravity = 30f;
	private float jump_force = -900f;
	private Vector2 up_direction = Vector2.Up;
	private bool moving = false;
	private bool jumping = false;
	private bool moveRight = true;
	private float min_X = -3090f, max_X = 3058f;

	private AnimatedSprite animation;


    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        animation = GetNode<AnimatedSprite>("Animation");
    }

	//called every frame. delta is the elapsed time since previous frame
	public override void _PhysicsProcess(float delta)
	{ 
		movement.y += gravity;
		if(Position.x <= max_X || Position.x >= min_X){
			PlayerMovement();
		}
	}

	void PlayerMovement(){
		if(Input.IsActionPressed("move_right") && Input.IsActionPressed("jump")){
			if(IsOnFloor()){
				movement.y	= jump_force;
				AnimateMovement(true,true,false);
			}
			else{
				movement.x = move_speed;
				AnimateMovement(true,false,true);
			}
		}
		else if(Input.IsActionPressed("move_left") && Input.IsActionPressed("jump")){
			if(IsOnFloor()){
				movement.y	= jump_force;
				AnimateMovement(true,true,false);
			}
			else{
				movement.x = -move_speed;
				AnimateMovement(true,false,false);
			}
		}
		else if(Input.IsActionPressed("move_right")){
			movement.x = move_speed;
			AnimateMovement(true,false,true);
		}
		else if(Input.IsActionPressed("move_left")){
			movement.x = -move_speed;
			AnimateMovement(true,false,false);
		}
		else if(Input.IsActionPressed("jump")){
			if(IsOnFloor()){
				movement.y	= jump_force;
				AnimateMovement(true,true,true);
			}
		}
		else{
			movement.x = 0f;
			AnimateMovement(false,true,true);
		}
		
		movement = MoveAndSlide(movement, up_direction);
	}

	void AnimateMovement(bool moving, bool jumping, bool moveRight){
		if(moving){
			if(!jumping){
				animation.Play("Run");	
			}
			else{
				animation.Play("Jump");
			}
			
			if(moveRight){
				animation.FlipH = false;
			}
			else{
				animation.FlipH = true;
			}
		}
		else{
			animation.Play("Idle");
		}
	}

	private void _on_Body_entered(PhysicsBody2D body)
	{
	    if(body.IsInGroup("Enemies")){
			GetParent().GetNode<CameraFollow>("Main Camera").playerDied = true;
			GetParent<GamePlay>().PlayerDied();
			QueueFree();
		}
	}

} //class



