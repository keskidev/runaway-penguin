using Godot;
using System;

public class HalloweenHud : CanvasLayer
{
//    [Signal]
//	public delegate void StartGame();
	
	public override void _Ready(){
		float score = GetParent<HalloweenScene>().GemCount;
		GetNode<Label>("GemCountlabel").Text = score.ToString();
		if(score == 1){
			ShowMessage("Pumpkin Collected.");
		}
		else{
			ShowMessage("Pumpkins Collected.");
		}
	}
	
	public void ShowMessage(string text){
		var messageLabel = GetNode<Label>("MessageLabel");
		messageLabel.Text = text;
		messageLabel.Show();
	}
	
	public void ShowGameOver(){
		ShowMessage("Gems collected.");
	    var messageLabel = GetNode<Label>("MessageLabel");
	    messageLabel.Text = "Dodge the\nCreeps!";
	    messageLabel.Show();
	
	    GetNode<Button>("StartButton").Show();	
	}
	
	public void UpdateScore(float score){
		GetNode<Label>("GemCountlabel").Text = score.ToString();
	}
	
	private void _on_PlayAgainButton_pressed()
	{
	    GetNode<Button>("PlayAgainButton").Hide();
		GetParent<HalloweenScene>().RestartGame();
		QueueFree();
	}
	
	private void _on_MenuButton_pressed()
	{
	    GetTree().ChangeScene("res://Scenes/TitleScreen.tscn");
	}
}









