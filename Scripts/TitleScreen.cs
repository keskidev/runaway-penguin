using Godot;
using System;

public class TitleScreen : Node2D
{
	private void _on_Play_pressed()
	{
		GetTree().ChangeScene("res://Scenes/Halloween.tscn");
	}
	
	private void _on_PlayIceLevel_pressed()
	{
	    GetTree().ChangeScene("res://Scenes/GamePlay.tscn");
	}
	
	private void _on_HowTo_pressed()
	{
	    GetTree().ChangeScene("res://Scenes/HowTo.tscn");
	}
	
	
}






