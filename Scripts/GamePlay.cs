using Godot;
using System;

public class GamePlay : Node
{
	[Export]
	private PackedScene harpoon1, harpoon2, walrus, snowball, gem, endScreen;
	
	public float GemCount = 0f;
	private Vector2 spawn_left = new Vector2(-3900f,400f);
	private Vector2 spawn_right = new Vector2(3915f,400f);
	private Timer restart;
	private Timer GemStart;
	
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
		GemStart = GetNode<Timer>("GemTimer");
		GemStart.Start();
     	restart = GetNode<Timer>("Restart");
    }
	
	private void _on_Enemy_Spawn()
	{
	    int randEnemy = new Random().Next(0,4);
		
		Enemies newEnemy = null;
		
		switch(randEnemy){
			case 0:
				newEnemy = harpoon1.Instance() as Enemies;
				break;
			case 1:
				newEnemy = harpoon2.Instance() as Enemies;
				break;	
			case 2:
				newEnemy = walrus.Instance() as Enemies;
				break;
			case 3:
				newEnemy = snowball.Instance() as Enemies;
				break;
		}
		
		Vector2 temp;
		
		if(new Random().Next(0,2) > 0){
			temp = spawn_right;
			newEnemy.moveRight = false;
		}
		else{
			temp = spawn_left;
			newEnemy.moveRight = true;
		}
		
		newEnemy.SetPosition(temp);
		AddChild(newEnemy);
	}
	
	private void _on_Gem_Spawn()
	{
    	Gem score_gem = null;
		score_gem = gem.Instance() as Gem;
		float position_x = new Random().Next(-3810,3580);
		float position_y = new Random().Next(300, 460);
		
		Vector2 gemLocation = new Vector2(position_x, position_y);
		
		score_gem.SetPosition(gemLocation);
		AddChild(score_gem);
	}
	
	
	public void PlayerDied(){
		HUD endScreen_Hud = null;
		endScreen_Hud = endScreen.Instance() as HUD;
		AddChild(endScreen_Hud);
	}
	
	public void RestartGame(){
		restart.Start();
	}

	void _on_Player_Died(){
		GetTree().ReloadCurrentScene();	
	}
	
}
