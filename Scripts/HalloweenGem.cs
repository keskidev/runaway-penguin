using Godot;
using System;

public class HalloweenGem : KinematicBody2D
{
	private float position_x;
	private float position_y;
	
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        //set the location of the gem
		position_x = new Random().Next(-3810,3580);
		position_y = new Random().Next(300, 460);
    }
	
	private void _on_HalloweenGem_body_entered(PhysicsBody2D body)
	{
	    if(body.IsInGroup("Player")){
			float newCount = GetParent<HalloweenScene>().GemCount + 1;
			GetParent<HalloweenScene>().GemCount = newCount;
			GetParent().GetNode<CanvasLayer>("GUI").GetNode<Label>("GemCount").Text = newCount.ToString();
			//update the UI
			QueueFree();
			GetParent().GetNode<Timer>("GemTimer").Start();
		}
	}
}
