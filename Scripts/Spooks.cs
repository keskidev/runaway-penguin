using Godot;
using System;

public class Spooks : KinematicBody2D
{
    private float moveSpeed = 450f;
	private float gravity = 20f;
	private Vector2 movement;
	public bool moveRight;
	//-1 == moving left ::: 1 == moving right
	public float direction = -1;
	public bool spook;
	private float min_X = -1160f, max_X = 2335f;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		if(this.Name.Contains("Wolf") || this.Name.Contains("Ghost")){
			spook = false;
		}
		else{
			spook = true;
		}
			
			
		if(moveRight){
			GetNode<AnimatedSprite>("Animation").FlipH = true;
		}
		else{
			moveSpeed *= -1f;
		}
		
		GetNode<AnimatedSprite>("Animation").Play("default");
		
	}

	  // Called every frame. 'delta' is the elapsed time since the previous frame.
	  public override void _PhysicsProcess(float delta)
	  {
			if(!spook){
				movement.y += gravity;
			}
			
			movement.x = moveSpeed;
				
			movement = MoveAndSlide(movement);
			
			if(Position.x > max_X || Position.x < min_X){
				QueueFree();
			}
	  }
	
}
