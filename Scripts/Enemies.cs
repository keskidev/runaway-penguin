using Godot;
using System;

public class Enemies : KinematicBody2D
{
	private float moveSpeed = 450f;
	private float gravity = 20f;
	private Vector2 movement;
	public bool moveRight;
	public bool isProjectile;
	private float min_X = -3900, max_X = 3915f;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
		if(this.Name.Contains("walrus")){
			isProjectile = false;
		}
		else{
			isProjectile = true;
		}
			
			
		if(moveRight){
			GetNode<AnimatedSprite>("Animation").FlipH = true;
		}
		else{
			moveSpeed *= -1f;
		}
		
	}

	  // Called every frame. 'delta' is the elapsed time since the previous frame.
	  public override void _PhysicsProcess(float delta)
	  {
			if(!isProjectile)
				movement.y += gravity;
				
			movement.x = moveSpeed;
			
			movement = MoveAndSlide(movement);
			
			if(Position.x > max_X || Position.x < min_X){
				QueueFree();
			}
	  }
} //Class