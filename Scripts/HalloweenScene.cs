using Godot;
using System;

public class HalloweenScene : Node
{
    [Export]
	private PackedScene bat, ghost, wolf, gem, endScreen;
	
	public float GemCount = 0f;
	private Vector2 spawn_left = new Vector2(-1160f,400f);
	private Vector2 spawn_right = new Vector2(2335f,400f);
	private Timer restart;
	private Timer GemStart;
	
	// Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
		GemStart = GetNode<Timer>("GemTimer");
		GemStart.Start();
     	restart = GetNode<Timer>("Restart");
    }

	public void PlayerDied(){
		HalloweenHud endScreen_Hud = null;
		endScreen_Hud = endScreen.Instance() as HalloweenHud;
		AddChild(endScreen_Hud);
	}
	
	public void RestartGame(){
		restart.Start();
	}
	
	private void _on_Gem_Spawn()
	{
	    HalloweenGem score_gem = null;
		score_gem = gem.Instance() as HalloweenGem;
		float position_x = 100;
		float position_y = 100;
		var screen = new Random().Next(0,3);
		switch(screen){
			case 0:
					position_x = new Random().Next(-1240,-100);
					position_y = new Random().Next(140,410);
				break;
			case 1:
					position_x = new Random().Next(-4,1175);
					position_y = new Random().Next(200, 535);
				break;
			case 2:
					position_x = new Random().Next(-3810,490);
					position_y = new Random().Next(150, 500);
				break;
		}
		Vector2 gemLocation = new Vector2(position_x, position_y);

		score_gem.SetPosition(gemLocation);
		AddChild(score_gem);
	}


	private void _on_Player_Died()
	{
	    GetTree().ReloadCurrentScene();	
	}
	
	private void _on_SpookTimer_timeout()
	{
	    int randEnemy = new Random().Next(0,3);

		Spooks newEnemy = null;

		switch(randEnemy){
			case 0:
				newEnemy = bat.Instance() as Spooks;
				break;
			case 1:
				newEnemy = ghost.Instance() as Spooks;
				break;	
			case 2:
				newEnemy = wolf.Instance() as Spooks;
				break;
		}

		Vector2 temp;

		if(new Random().Next(0,2) > 0){
			temp = spawn_right;
			newEnemy.moveRight = false;
		}
		else{
			temp = spawn_left;
			newEnemy.moveRight = true;
		}

		newEnemy.SetPosition(temp);
		AddChild(newEnemy);
	}
}


